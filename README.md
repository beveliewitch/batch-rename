# PyPic batch rename

A little program that aim to rename all pictures in a directory with the date and name of camera.

# How to use it

Use this following command line :

`python __main__.py -idir "pathOfInputDirectory" -odir "pathOfOutputDirectory" -name "name of pictures"`

Or just run using `python __main__.py` to access to GUI application.

All pictures of the directory will be renamed under the following format : "Ymd-HMS-CAMERA Name"
